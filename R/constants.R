# define constants for plotting haplohseq results

cytobands_file = "~/box/Data/genomes/hg19/cytoBand.txt"
chr_arms_file = "~/box/Data/genomes/hg19/hg19.chromosome_arms.bed"
chr_arms_df = read.table(chr_arms_file, header=FALSE, sep="\t")
colnames(chr_arms_df) = c("chrom","begin","end","chr_arm")
chr_arms_df$chrom = gsub("chr", "", chr_arms_df$chrom)
# TODO: why exclude some arms?
exclude_arms = c("13p","14p","15p","22p","Yp","Yq")
chr_arms = chr_arms_df$chr_arm[!(chr_arms_df$chr_arm %in% exclude_arms)]

chrs = c("1","2","3","4","5",
         "6","7","8","9","10",
         "11","12","13","14","15",
         "16","17","18","19","20",
         "21","22","X")

# hg19 chr boundaries
chrLen = c()
chrLen["1"] <- 249250621
chrLen["2"] <- 243199373
chrLen["3"] <- 198022430
chrLen["4"] <- 191154276
chrLen["5"] <- 180915260
chrLen["6"] <- 171115067
chrLen["7"] <- 159138663
chrLen["8"] <- 146364022
chrLen["9"] <- 141213431
chrLen["10"] <- 135534747
chrLen["11"] <- 135006516
chrLen["12"] <- 133851895
chrLen["13"] <- 115169878
chrLen["14"] <- 107349540
chrLen["15"] <- 102531392
chrLen["16"] <- 90354753
chrLen["17"] <- 81195210
chrLen["18"] <- 78077248
chrLen["19"] <- 59128983
chrLen["20"] <- 63025520
chrLen["21"] <- 48129895
chrLen["22"] <- 51304566
chrLen["X"] <- 155270560
chrLen["Y"] <- 59373566

# set plot chromsome cumulative coordinates
plot_chr_pos_hg19 = c()
padded_pos = 1
for (i in names(chrLen)) {
  plot_chr_pos_hg19[i] <- padded_pos
  padded_pos = padded_pos + as.numeric(chrLen[i])
}

# hg38 chr boundaries
chrLen = c()
chrLen["1"] <- 248956422
chrLen["2"] <- 242193529
chrLen["3"] <- 198295559
chrLen["4"] <- 190214555
chrLen["5"] <- 181538259
chrLen["6"] <- 170805979
chrLen["7"] <- 159345973
chrLen["8"] <- 145138636
chrLen["9"] <- 138394717
chrLen["10"] <- 133797422
chrLen["11"] <- 135086622
chrLen["12"] <- 133275309
chrLen["13"] <- 114364328
chrLen["14"] <- 107043718
chrLen["15"] <- 101991189
chrLen["16"] <- 90338345
chrLen["17"] <- 83257441
chrLen["18"] <- 80373285
chrLen["19"] <- 58617616
chrLen["20"] <- 64444167
chrLen["21"] <- 46709983
chrLen["22"] <- 50818468
chrLen["X"] <- 156040895
chrLen["Y"] <- 57227415

# set plot chromsome cumulative coordinates
plot_chr_pos_hg38 = c()
padded_pos = 1
for (i in names(chrLen)) {
  plot_chr_pos_hg38[i] <- padded_pos
  padded_pos = padded_pos + as.numeric(chrLen[i])
}

